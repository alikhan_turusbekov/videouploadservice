import os
import shutil
from datetime import datetime

from fastapi import FastAPI, Depends, HTTPException, UploadFile, File, Form, BackgroundTasks
from sqlalchemy.orm import Session

from database import get_db
from crud import crud
from authendpoints import auth, auth_handler
import models

app = FastAPI()
app.include_router(auth)
app.include_router(crud)


@app.get('/')
def about_page():
    return {'message': 'hello! This service is made for the video upload.'
                       'The requirements: user should be registered and then authenticated to upload files'
                       'File size should be less than '
                       'User view only his/her own videos'}


def saveFile(video: models.Video, file: UploadFile):
    with open(f'{video.path}', 'wb') as buffer:
        shutil.copyfileobj(file.file, buffer)


@app.post('/uploadVideo', status_code=201)
async def protected(background_tasks: BackgroundTasks,
                    username=Depends(auth_handler.auth_wrapper),
                    title: str = Form(...), file: UploadFile = File(...),
                    db: Session = Depends(get_db)):
    file.file.seek(0, os.SEEK_END)
    if file.file.tell() > 209715200:
        raise HTTPException(status_code=400, detail='The video is larger than 200MB')

    now_time = datetime.now().strftime("%Y.%m.%d.%H.%M.%S")
    owner = db.query(models.User).filter(models.User.username == username).first()
    video = models.Video(
        key=owner.username + now_time,
        filename=title,
        filesize=file.file.tell(),
        path='resources/uploadedVideos/' + owner.username + now_time,
        owner_id=owner.uid
    )
    background_tasks.add_task(saveFile, video, file)
    db.add(video)
    db.commit()
    db.refresh(video)
    return video
