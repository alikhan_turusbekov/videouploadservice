from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from auth import AuthHandler
from database import get_db
from schemas import AuthDetails
import models

auth = APIRouter()
auth_handler = AuthHandler()


@auth.post('/register', status_code=201)
def register(auth_details: AuthDetails, db: Session = Depends(get_db)):
    if not db.query(models.User).filter(models.User.username == auth_details.username):
        raise HTTPException(status_code=400, detail='Username is taken')
    hashed_password = auth_handler.get_password_hash(auth_details.password)
    db_user = models.User(username=auth_details.username, password_hash=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


@auth.post('/login')
def login(auth_details: AuthDetails, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.username == auth_details.username).first()
    if (user is None) or (not auth_handler.verify_password(auth_details.password, user.password_hash)):
        raise HTTPException(status_code=401, detail='Invalid username and/or password')
    token = auth_handler.encode_token(user.username)
    return {'token': token}
