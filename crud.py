from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from auth import AuthHandler
from database import get_db
from schemas import VideoDetails
import models

crud = APIRouter()
auth_handler = AuthHandler()


@crud.get('/users')
def get_all_users(db: Session = Depends(get_db)):
    users = db.query(models.User).all()
    return users


@crud.delete('/delete/user/{user_id}')
def delete_user(user_id: int, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.uid == user_id).first()
    if user is None:
        raise HTTPException(status_code=401, detail='No user was found')
    db.delete(user)
    db.commit()
    return {"message": "User was deleted"}


@crud.get('/all-videos')
def get_all_videos(db: Session = Depends(get_db)):
    videos: list[models.Video] = db.query(models.Video).all()
    video_details: list[VideoDetails] = []
    for i in range(len(videos)):
        v = VideoDetails()
        v.filename = videos[i].filename
        v.filesize = videos[i].filesize
        video_details.append(v)
    return video_details


@crud.delete('/delete/video/{video_key}')
def delete_user(video_key: str, db: Session = Depends(get_db)):
    video = db.query(models.Video).filter(models.Video.key == video_key).first()
    if video is None:
        raise HTTPException(status_code=401, detail='No video was found')
    db.delete(video)
    db.commit()
    return {"message": "Video was deleted"}


def get_user_id_by_username(username: str, db: Session):
    user = db.query(models.User).filter(models.User.username == username).first()
    return user.uid


@crud.get('/videos/')
def get_user_videos(username=Depends(auth_handler.auth_wrapper),
                    db: Session = Depends(get_db)):
    videos_uploaded: list[models.Video] = []
    uid = get_user_id_by_username(username, db)
    videos: list[models.Video] = db.query(models.Video).all()
    for element in videos:
        if element.owner_id == uid:
            videos_uploaded.append(element)
    return videos_uploaded
