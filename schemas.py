from pydantic import BaseModel


class AuthDetails(BaseModel):
    username: str
    password: str


class VideoDetails:
    filename: str
    filesize: int
