from database import Base, engine
from models import User, Video

print("Creating database ....")

Base.metadata.create_all(engine)
