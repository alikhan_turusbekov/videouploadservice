from database import Base
from sqlalchemy import String, Integer, Column, ForeignKey


class User(Base):
    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True)
    username = Column(String(30), nullable=False, unique=True)
    password_hash = Column(String(128), nullable=False)


class Video(Base):
    __tablename__ = 'videos'

    key = Column(String, primary_key=True)
    filename = Column(String(255), nullable=False, unique=True)
    filesize = Column(Integer, nullable=False)
    path = Column(String, nullable=False)
    owner_id = Column(Integer, ForeignKey(User.uid))
